# Hydra workshop

### English
Hydra is a WEB application for live coding of graphical animations. The ingredients are generative graphics, camera images and digital feedback loops.

This is my material collection for a Hydra workshop.

First, the principles on which Hydra is based are introduced. Then it is about how to code in Hydra. With examples to try out.

[Hydra-Workshop English.](./hydra-workshop-en.md)

### Deutsch
Hydra ist eine WEB Applikation um grafische Animationen live zu programmieren. Die Zutaten sind generative Graphiken, Kamerabilder und digitale Rückkopplungsschleifen.

Dies ist meine Materialsammlung für einen Hydra-Workshop.

Zuerst werden die Mechanismen auf denen Hydra basiert vorgestellt. Anschließend geht es darum wie Hydra programmiert wird. Mit Beispielen zum ausprobieren.

[Hydra-Workshop Deutsch.](./hydra-workshop-de.md)

***

Author Thomas Jourdan: https://chaos.social/@kandid

License: CC BY-NC-SA 4.0
