# Hydra Video Synthesizer

[[_TOC_]]

## Show case
*What happens when you point a VHS camera at a TV monitor that displays the image from that camera?*

***

## From where does it came?


- analog modular synthesizer for sound
  - voltage controlled oscillators
  - audio filters

- analog TV equipment
  - analog video mixers

- feedback loops
  - digital or analog camera monitor feedback loops
  - analog audio "no input mixer"

***

## Basics

### Pixel
The color value of all pixels is recalculated at 30 or 60 frames per second.

As long as a single frame of the animation is drawn, the same calculation formula applies to all pixels contributing to a buffer.

- Input:
  - Location `x`, `y`,
  - Time: `time`.
  - Audio: `fft`.
  - optional: RGB value taken from the upstream buffer
- Output:
  - new RGB value of this pixel


### Buffer
Graphic data are constantly copied from one buffer to the next buffer. During this copying, the per pixel calculation takes place.

There are exactly 4 buffers in Hydra (from the user's point of view).

The data flow through the buffers may contain loops. A directed graph with cycles. This allows feedback loops.

### live code
The calculation formula can be changed at any time. The data in the buffers are not lost when the code is updated. No edit- compile- run- workflow is necessary.

Functional programming languages are used. Chaining and nesting of functions will transform the graphic during the data flow.

### Authenticity
Source code and error messages are visible in addition to the animation. "Show you screens".

***

## Synthesis
Most of these synthesis techniques apply to sound and visuals in a similar way. These synthesis techniques are for analog and digital systems useful.

### Additive Synthesis
For audio and visuals.

Superposition of oscillators of different frequency and phase.

### Subtractive Synthesis
#### Suitable for audio
From noise, select specific frequency bands through filters.

#### For visuals
Noise functions are used that do not produce high 'spatial' frequencies.

* Example: Perlin Noise

Or noise functions where you can influence how much high frequencies are wanted.

* Example: Turbulence based on Perlin Noise

### Granular Synthese
* Audio: "Sound Grains".
* Visuals: Stroboscopic effects.


## Sources of graphical data for Hydra
* generative "oscillators" `osc()`
* buffers within Hydra `src(o0)`
* cameras `s0.initCam()`
 * emulated cameras "v4l2loopback" loop back devices `s0.initCam()`
* the content of windows from other applications `s0.initScreen()`
* processing p5js `s0.init({ src: myp5.canvas })`
* and several other javascript libraries

### Feedback loops
The complexity arises from feedback loops, even if we have simple arithmetic expressions.

This works for audio and visuals in general.


### Transformation and distortion of the Cartesian coordinate system
Mapping the vector space `R2 -> R2`

* scale, rotate, ....

* In combination with a feedback loop this approach leads to fractals.

### Constants as parameters / Lambda functions as parameters
Use an expression to calculate the value of one parameter and then ...

* -> then pass the result.
  * The result is used as a constant.

* -> or bind a function `() => ...` to this parameter.
  * Here the result is a function. This function is called per frame.

Functional programming languages: Variables can contain constants or (lambda) functions.

```javascript
// 3+1 is calculated when this line is sent during live coding.
voronoi(5, 0, 3+1).out(o0)
```

```javascript
// 3+Math.sin(time) is calculated when this line is sent during live coding.
// The global variable time is taken into account in this calculation. But only once.
voronoi(5, 0, 3+Math.sin(time)).out(o0)
```

```javascript
// () => 3+Math.sin(time) is calculated when this line is sent during live coding.
// () => 3+Math.sin(time) is bound as a function.
// This function is recalculated before each frame.
voronoi(5, 0, () => 3+Math.sin(time)).out(o0)
```

***

## Different (or similar) systems

### Paloma Kop
Analog Video Sythesizer.

https://palomakop.tv

"From Material Space(Time) into Electronic Spaces(Time)" This master thesis originates from the application area of analog signal processing. But it is transferable to digital (feedback) systems. Highly recommended, even when you work always with digital systems.
https://palomakop.s3.amazonaws.com/palomakop_thesis_book_digital.pdf

### scanlines
"Discussion group for diy video and audio projects." Digital and Analog.

https://scanlines.xyz

### analog Not analog
Live coding visuals. Made with openFrameworks and Clojure.

https://metagrowing.org/

The 'analog Not analog' cookbook.

http://metagrowing.org/cookbook/

### SuperCollider
For sound synthesis and analysis. In this context you can use SuperCollider for "color to sound" experiments. Or as a low frequency oscillator to control a graphic animation. SuperCollider can also be used as a live coding tool.

https://supercollider.github.io/

### Tidal Cycles
Live coding sound. Based on Haskel and SuperCollider.

https://tidalcycles.org/

### "No Input Mixer" Tutorial by Sarah Belle Reid
An analog audio mixer is used as a sound generator. The outputs of the mixer go back into the mixer as inputs. The analog electronics generate thermal noise. This noise is the initial source. A feedback loop and a filter or a equalizer is creating sound.

This way of thinking is very transferable to visual feedback loops.

https://www.youtube.com/watch?v=oUhfkaVUPY8

### Sonic Pi
Live coding sound.
https://sonic-pi.net/


***

## System requirements for Hydra
* A browser that supports WebGL.
* Access to the internet to start Hydra.
* Or: Hydra as a plugin for the Atom Editor.

***

## Links

Hydra can be found here:
https://hydra.ojack.xyz/

#### Olivia Jack
Olivia Jack has developed Hydra.
https://ojack.xyz/

#### Introduction to Hydra

https://hydra.ojack.xyz/docs/#/getting_started

https://github.com/hydra-synth/hydra

Description and examples of the functions:
https://hydra.ojack.xyz/api/

Description of the functions:
https://github.com/ojack/hydra/blob/main/docs/funcs.md

#### hydra internet garden
https://hydra.ojack.xyz/garden/

#### Extensions
Library with additional functions: https://gitlab.com/metagrowing/extra-shaders-for-hydra

hyper-hydra extensions: https://github.com/geikha/hyper-hydra/tree/main

#### Hydra for 3 persons
"This is a collaborative console for hydra synth" https://pixeljam.glitch.me

#### Hydra Discord server
https://discord.gg/ZQjfHkNHXC

***
## Shotcut key
* `CTRL-Enter`: run one line of code
* `CTRL-Shift-Enter`: run all code
* `CTRL-Shift-H`: hide the source code
* `CTRL-Shift-S`: make a screenshot

***
## Examples
### Minimal example
This is the minimal example. A sine oscillator writes to buffer 'o0'.

![](md-images/hydra-2023-10-12-21.5.47.png)

```javascript
osc().out()
```
The sine oscillator can be configured with parameters.

![](md-images/hydra-2023-10-12-21.29.37.png)

`osc( frequency, sync, offset )`

* `frequency` :: float (default `60.0`)
* `sync` :: float (default `0.1`)
* `offset` :: float (default `0.0`)

Documentation of the parameters: https://github.com/ojack/hydra/blob/main/docs/funcs.md

```javascript
osc(15, 0.1, 0.5).out()
```

### Geometry
Transforming the coordinate system.

![](md-images/hydra-2023-10-12-21.40.48.jpg)

```javascript
render()
voronoi().out(o0)
voronoi().scale(0.5).kaleid().out(o1)
voronoi().repeat().out(o2)
voronoi().pixelate().out(o3)
```

### Color
Manipulating the color.

![](md-images/hydra-2023-10-26-19.48.24.jpg)

```javascript
render()
voronoi().out(o0)
voronoi().colorama(0.55).out(o1)
voronoi().color(1,0,3).out(o2)
voronoi().thresh().out(o3)
```

### Attention: sequence of processing steps
0. coordinate system
1. transformation of coordinates
2. map coordinates to grayscale (or color)
3. manipulate this color

There is a coordinate system. This is transformed. Then the colors or grayscales are calculated from the transformed coordinates. In the last step the color can be changed.

Attention: The notation does not necessarily suggest this order.

```javascript
osc()              // 2. map coordinates to color or gray-scale
	.rotate(0.7)      // 1. transform coordinate system
	.colorama(0.55)  // 3. change color
	.out(o0)
```

![](md-images/hydra-2023-10-26-20.8.40.png)

### Kamera
Include the image stream coming from the camera.

![](md-images/camera-4.jpg)

```javascript
s0.initCam()
render()
src(s0).hue().out(o0)
src(s0).thresh(0.5).out(o1)
src(s0).shift().out(o2)
src(s0).color(1.7, 0.5, 0, 1).out(o3)
```
If there are several cameras, the required camera is selected by parameter: `0`, `1`, ...
* `s0.initCam(0)`
* `s0.initCam(1)`

```javascript
s0.initCam(0)
s1.initCam(1)
src(s0).blend(s1).out(o2)
render(o2)
```

### Forward Pipeline
Graphic data are flowing through the buffers without feedback loops.

A --> B "directed acyclic graph".

#### Upstream is superimposed
These functions are similar to the 'blending modes' in graphics programs to combine 'layers'.

![](md-images/hydra-2023-10-12-21.43.13.jpg)

```javascript
render()
osc(4, 0.5, 5).rotate(() => time).out(o0)
osc(4, () => 0.5+0.1*Math.sin(0.01*time), 2).rotate(0.5).out(o1)
src(o0).diff(src(o1).repeat(10,1)).out(o2)
src(o0).blend(noise(3)).out(o3)
```

Data flow simplified, the geometric transformations are removed here.

![](flow/upstream_ueberlagert_small.png)

#### Upstream modulates
The modulator way of thinking comes from the world of analog modular synthesizers. Example: A very low frequency oscillator (LFO) modulates the frequency of a sine wave generator that oscillates in the audible range.

![](md-images/hydra-2023-10-13-6.40.8.jpg)

```javascript
render()
osc(5, 0.5, 5).rotate(() => time).out(o0)
osc(4, () => 0.5+0.1*Math.sin(0.01*time), 2).modulateKaleid(voronoi()).out(o1)
src(o0).pixelate().modulatePixelate(src(o1)).out(o2)
src(o0).modulateRotate(noise()).out(o3)
```
![](flow/upstream_moduliert_small.png)

### Feedback loops
There are feedback loops between buffers allowed.

A <--> B "Directed graph, cycles are allowed".

With feedback loops the effects are not easily controllable, but very interesting.

#### Feedback loops with external camera
What happens when you point the camera at the monitor?

![](md-images/hydra-2023-10-14-12.51.55.jpg)

```javascript
s0.initCam()
//src(s0).out(o0)
src(s0).colorama(0.3).blend(o0, 0.95).out(o0)
```
![](flow/feedback_extern_1_small.png)

#### Internal Feedback loops
Short feedback loops with the own previous state. Here the buffers are coupled with the "inverted" previous state or with the "difference" to the previous state.

![](md-images/Screenshot_20231118_202057.jpg)

```javascript
s1.initCam()

render()

render(o3)

src(s1).out(o1)
src(o1).scale(3).diff(src(o2).scale(0.97).rotate(1.5)).out(o2)
src(o2).blend(src(o3), 0.95).out(o3)
```
![](flow/feedback_intern_1_small.png)

#### Feedback: Suppress noise
This short feedback loop at the end of the pipeline suppresses noise.

`src(o2).blend(src(o3), 0.95).out(o3)` is similar to a lowpass filter. Only 5% of the current "upstream" image `o2` is combined with 95% of the old state from `o3` and written to the accumulator `o3`.

![](md-images/hydra-2023-10-14-13.1.48.jpg)

```javascript
render(o3)
shape(()=>3+2.5*Math.sin(0.31*time)).colorama(0.2).saturate(0.7).hue(()=>Math.sin(time)).out(o0)
src(o0).scale(3).diff(src(o2).scale(0.97).rotate(1.5)).out(o2)
src(o2).blend(src(o3), 0.95).out(o3)
```
![](flow/feedback_2_sources_small.png)

### Feedback and Modulator

![](md-images/hydra-2023-10-14-16.15.10.jpg)

Also the approach working with modulators in a feedback loop to manipulate the graphical can be used.

This example generates noise. It does not contain any feedback loop for damping.

```javascript
render()
osc(7, 0.4, 2).diff(src(o1)).saturate(0.3).hue(0.5).out(o0)
osc(7, 0.4, 2).modulateRepeat(src(o2).rotate(0.3)).saturate(0.3).hue(0.05).out(o1)
osc(7, 0.4, 2).modulateRepeat(src(o3).rotate(0.3).color(1.5)).saturate(0.3).hue(0.05).out(o2)
src(o2).modulateRotate(src(o0).scale(0.25).repeat(2)).colorama(0.5).out(o3)
```
![](flow/feedback_intern_modulator_small.png)

### Fraktale per Feedback erzeugen
30 years back: "Mehrfach-Verkleinerungs-Kopier-Maschine"

![](md-images/Screenshot_20231022_201859.png)

```javascript
//s0.initCam()
//src(s0).out(o0)

osc()
	.thresh(0.1)
	.out(o0)

src(o0)
	.mask(shape(5, 0.95))
	.invert()
	.rotate()
	.scale(0.99)
	.out(o0)

src(o0)
	.mask(shape(3, 1.1))
	.colorama(0.01)
	.rotate(() => 0.05 * time)
	.scale(() => 0.95 + 0.04 * Math.sin(0.09 * time))
	.scrollY(0.05)
	.out(o0)
  ```

### Define additional fragment shaders
#### Motivation
* additional patterns
* additional color mixers
* show your own distinctive style

#### Conditions
* `setFunction()` will define WebGL 1.0 fragment shaders.
* Your additional code must fit exactly into the scheme of the Hydra code generator.

#### GLSL Fragment Shader Code
When using `setFunction()` to develop custom FragmentShader code, it must syntactically match exactly the Fragment Shader code generated by `hydra-synth`.

Creating a custom function does not mean that you can create any GLSL function.

Functions are divided into 5 different types predefined in the Hydra Code Generator.
* src
* coord
* color
* combine
* combineCoords

Here you can find the predefined functions. Use this as a template to get started. https://github.com/hydra-synth/hydra-synth/blob/main/src/glsl/glsl-functions.js


#### Example: concentric rings

![](md-images/hydra-2023-10-14-17.48.12.jpg)

```javascript
setFunction({
  name: 'concentric',
  type: 'src',
  inputs: [
    {name: 'base',     type: 'float', default: 5.0},
    {name: 'octaves',  type: 'float', default: 2.0},
    {name: 'ampscale', type: 'float', default: 0.5},
    {name: 'speed',    type: 'float', default: 1.0},
  ],
  glsl: `
  float r = base*length(_st - vec2(0.5));
  float d = 0.5*sin(r+speed*time) + 0.5;
  d += ampscale*(0.5*sin(r*octaves+speed*time) + 0.5);
  octaves *= octaves;
  ampscale *= ampscale;
  d += ampscale*(0.5*sin(r*octaves) + 0.5);
  octaves *= octaves;
  ampscale *= ampscale;
  d += ampscale*(0.5*sin(r*octaves+speed*time) + 0.5);
  return vec4(d, d, d, 1);
`})

render(o0)
concentric(30).out(o0)

render(o1)
concentric(5,2,0.5).modulate(concentric(20,2,0.5)).out(o1)

render(o2)
concentric(5,4,0.25).color(0.7,0,0).add(concentric(3,2,0.5).scrollY(0.1).color(0.0,0.6,0)).out(o2)

render(o3)
concentric(30,2,0.5,1.31).colorama(0.5).modulateRepeat(concentric(30,2,0.5,0.51),0.1,0.1,0,0.5).out(o3)

render()
```

#### 2 ways to display GLSL code

![](md-images/dump-shader-source.png)

Write the generated source code to the WEB console.
```javascript
shader = osc(30, 0.1, 0.2).colorama(0.7).glsl(o0)
console.log(shader[0].frag)
```

Display the generated source code in the browser.
```javascript
await loadScript("https://hyper-hydra.glitch.me/hydra-debug.js")
osc().colorama().debug(o0)
```


### Libraries with additional fragment shaders

![](md-images/Screenshot_20231014_175608.jpg)

```javascript
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

blobs(0.13, 0.2, 0.2)
	.modulate(blobs(0.21, 0.5, 0.2)
		.modulate(blobs(0.23, 0.9, 0.2), 1), 1)
	.shift()
	.out(o0)

render(o0)
```
https://hydra.ojack.xyz/dev/

https://gitlab.com/metagrowing/extra-shaders-for-hydra

![](md-images/hydra_library.png)


### Processing / Integration of p5js in Hydra

![](md-images/hydra-2023-10-16-12.42.38.jpg)

```javascript
// see: https://p5js.org/examples/math-parametric-equations.html
setResolution(640, 640)
if(typeof myp5 === 'undefined') {
  // myp5 is globally visible
  // only one constructor call in case of rerunning this sketch
  myp5 = new P5({mode: 'WEBGL'})
}
let t = 0
let sl = 250
myp5.hide() //hide p5js canvas.

// the parameter at which x and y depends is usually taken as either t or symbol of theta
myp5.draw = () => {
  myp5.background('#e0e0e0');
  myp5.strokeWeight(3);
  myp5.stroke('#00ffff');
  myp5.line(x1(t), y1(t), x2(t) + 20, y2(t) + 20);
  myp5.stroke('#0f0f0f');
  for(let i = 1;i<100;i++){
    myp5.line(x1(t+i), y1(t+i), x2(t+i) + 20, y2(t+i) + 20);
  }
  t += 0.15;
}
// function to change initial x co-ordinate of the line
function x1(t){
  return Math.sin(t / 10) * sl + Math.sin(t / 20) * sl + Math.sin(t / 30) * sl;
}

// function to change initial y co-ordinate of the line
function y1(t){
  return Math.cos(t / 10) * sl + Math.cos(t / 20) * sl + Math.cos(t / 30) * sl;
}

// function to change final x co-ordinate of the line
function x2(t){
  return Math.sin(t / 15) * sl + Math.sin(t / 25) * sl + Math.sin(t / 35) * sl;
}

// function to change final y co-ordinate of the line
function y2(t){
  return Math.cos(t / 15) * sl + Math.cos(t / 25) * sl + Math.cos(t / 35) * sl;
}

s0.init({ src: myp5.canvas })
render(o0)
src(s0).out(o0)

render(o1)
src(s0).scale(0.5).kaleid(5).out(o1)

render(o2)
src(s0).diff(o1).out(o2)

render(o3)
src(o2).blend(o3, 0.9).out(o3)
```

### Animation reacts to sound
The microphone of the laptop serves as the source.

FFT "Fast Fourier Transformation": The audio signal is decomposed into its frequency components. Spectral components.

Hydra uses 4 frequency bands by default. `a.fft[0]` is the lowest frequency band. Details can be found here: https://github.com/hydra-synth/hydra#audio-responsiveness

![](md-images/Screenshot_20231028_204430.png)

```javascript
a.show()
shape(3)
	.modulateRotate(noise(), () => 5 * a.fft[0])
	.out(o0)
```

```javascript
voronoi(6, 3)
	.modulateKaleid(noise(1,() => 0.0005 * a.fft[0]), 13)
	.colorama(() => a.fft[3])
	.invert()
	.out(o0)
```

```javascript
let gain = 3
voronoi(5, () => 0.005 * a.fft[0], 7)
	.modulateScale(noise(), () => a.fft[0] * a.fft[1])
	.color(() => gain * 7 * a.fft[1],
           () => gain * 5 * a.fft[2],
           () => gain * 2 * a.fft[3])
	.out(o0)
```

***

Author Thomas Jourdan: https://chaos.social/@kandid

License: CC BY-NC-SA 4.0
