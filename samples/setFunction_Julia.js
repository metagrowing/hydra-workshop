setFunction({
  name: 'julia',
  type: 'src',
  inputs: [
    {name: 're',  type: 'float', default: 0.7454280},
    {name: 'im',  type: 'float', default: 0.1130063},
  ],
  glsl: `
  vec2 c = vec2(re, im);
  vec2 julia = (_st - vec2(0.5)) / 1.0;
  int count = 0;
  for(int i=0; i<32; i++) {
    julia = vec2(julia.x * julia.x - julia.y * julia.y,
                 2.0 * julia.x * julia.y)
            + c;
    if(dot(julia, julia) > 20.0) {
        count = i;
        break;
    }
  }
  float gray = 1.0 - sqrt(float(count) / 32.0);
  return vec4(gray, gray, gray, 1);
`})

await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-color.js")

julia().scale(0.5).rotate().out(o0)

julia(() => Math.sin(0.13*time), () => Math.sin(0.11*time)).scale(0.3).grarose(0.8).out(o0)

//julia(() => Math.sin(0.13*time), () => Math.sin(0.11*time)).scale(0.3).colorama(0.8).out(o0)

julia(() => Math.sin(0.13*time), () => Math.sin(0.11*time)).modulateScale(noise(), 0.1).colorama(0.8).out(o0)

await loadScript("https://hyper-hydra.glitch.me/hydra-debug.js")
julia(() => Math.sin(0.13*time), () => Math.sin(0.11*time)).scale(0.7).grarose(0.8).debug(o0)


