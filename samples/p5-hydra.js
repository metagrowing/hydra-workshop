// see: https://p5js.org/examples/math-parametric-equations.html
setResolution(640, 640)
console.log('0: ' + typeof myp5)
if(typeof myp5 === 'undefined') {
  // myp5 is globally visible
  // only one constructor call in case of rerunning this sketch
  myp5 = new P5({mode: 'WEBGL'})
  console.log('1: ' + typeof myp5)
}
let t = 0
let sl = 250
console.log('2: ' + typeof myp5)
myp5.hide() //hide p5js canvas.

// the parameter at which x and y depends is usually taken as either t or symbol of theta
myp5.draw = () => {
  myp5.background('#e0e0e0');
  myp5.strokeWeight(3);
  myp5.stroke('#00ffff');
  myp5.line(x1(t), y1(t), x2(t) + 20, y2(t) + 20);
  myp5.stroke('#0f0f0f');
  for(let i = 1;i<100;i++){
    myp5.line(x1(t+i), y1(t+i), x2(t+i) + 20, y2(t+i) + 20);
  }
  t += 0.15;
}
// function to change initial x co-ordinate of the line
function x1(t){
  return Math.sin(t / 10) * sl + Math.sin(t / 20) * sl + Math.sin(t / 30) * sl;
}

// function to change initial y co-ordinate of the line
function y1(t){
  return Math.cos(t / 10) * sl + Math.cos(t / 20) * sl + Math.cos(t / 30) * sl;
}

// function to change final x co-ordinate of the line
function x2(t){
  return Math.sin(t / 15) * sl + Math.sin(t / 25) * sl + Math.sin(t / 35) * sl;
}

// function to change final y co-ordinate of the line
function y2(t){
  return Math.cos(t / 15) * sl + Math.cos(t / 25) * sl + Math.cos(t / 35) * sl;
}

s0.init({ src: myp5.canvas })
render(o0)
src(s0).out(o0)

render(o1)
src(s0).scale(0.5).kaleid(5).out(o1)

render(o2)
src(s0).diff(o1).out(o2)

render(o3)
src(o2).blend(o3, 0.9).out(o3)
