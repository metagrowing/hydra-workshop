setFunction({
  name: 'concentric',
  type: 'src',
  inputs: [
    {name: 'base',     type: 'float', default: 5.0},
    {name: 'octaves',  type: 'float', default: 2.0},
    {name: 'ampscale', type: 'float', default: 0.5},
    {name: 'speed',    type: 'float', default: 1.0},
  ],
  glsl: `
  float r = base*length(_st - vec2(0.5));
  float d = 0.5*sin(r+speed*time) + 0.5;
  d += ampscale*(0.5*sin(r*octaves+speed*time) + 0.5);
  octaves *= octaves;
  ampscale *= ampscale;
  d += ampscale*(0.5*sin(r*octaves) + 0.5);
  octaves *= octaves;
  ampscale *= ampscale;
  d += ampscale*(0.5*sin(r*octaves+speed*time) + 0.5);
  return vec4(d, d, d, 1);
`})

render(o0)

concentric(30).out(o0)

concentric(5,2,0.5).modulate(concentric(20,2,0.5)).out(o1)

concentric(5,4,0.25).color(0.7,0,0).add(concentric(3,2,0.5).scrollY(0.1).color(0.0,0.6,0)).out(o2)

concentric(30,2,0.5,1.31).colorama(0.5).modulateRepeat(concentric(30,2,0.5,0.51),0.1,0.1,0,0.5).out(o3)
