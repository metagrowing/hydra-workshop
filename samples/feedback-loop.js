render(o3)
shape(()=>3+2.5*Math.sin(0.31*time)).colorama(0.2).saturate(0.7).hue(()=>Math.sin(time)).out(o0)
src(o0).scale(3).diff(src(o2).scale(0.97).rotate(1.5)).out(o2)
src(o2).blend(src(o3), 0.95).out(o3)

