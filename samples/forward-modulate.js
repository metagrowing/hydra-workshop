render()
osc(5, 0.5, 5).rotate(() => time).out(o0)
osc(4, () => 0.5+0.1*Math.sin(0.01*time), 2).modulateKaleid(voronoi()).out(o1)
src(o0).pixelate().modulatePixelate(src(o1)).out(o2)
src(o0).modulateRotate(noise()).out(o3)
