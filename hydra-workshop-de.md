# Hydra Video Synthesizer

[[_TOC_]]

## Show case
*Was geschieht wenn man eine VHS Kamera auf einen TV Monitor, der das Bild dieser Kamera anzeigt, ausrichtet?*

***

## Vorbilder

- analoge modulare Synthesizer für Sound
  - voltage controlled oscillators
  - Audiofilter

- analoges TV Equipment
  - analoge Video Mixer

- Rückkopplungsschleifen
  - digitale oder analoge Kamera Monitor Rückkopplungsschleifen
  - Analoge Audio "No Input Mixer"

***

## Grundlagen

### Pixel
Die aktuelle Farbe aller Pixel wird 30 oder 60 mal pro Sekunde neu berechnet.

Solange ein Frame der Animation gezeichnet wird gilt für alle Pixel eines Puffers die selbe Berechnungsformel.

- Eingabe:
  - Ort `x`, `y`,
  - Zeit: `time`
  - Audio: `fft`
  - optional: RGB Wert des upstream Puffers
- Ausgabe:
  - neuer RGB Wert des Pixels

### Puffer
Die Grafikdaten werden ständig von einem Puffer in den nächsten kopiert. Bei diesem Kopieren findet die Berechnung statt.

Es gibt aus Sicht des Anwenders genau in Hydra 4 Puffer.

Der Datenfluss durch die Puffer darf Schleifen enthalten. Gerichteter Graph mit Zyklen.

### live code
Die Berechnungsformel kann jederzeit geändert werden. Die Daten in den Puffern gehen dabei nicht verloren. Es ist kein edit- compile- run- Workflow notwendig.

Es werden funktionale Programmiersprachen eingesetzt. Verkettung und Schachtelung von Funktionen transformieren die Grafikdaten.

### Authentizität
Der Code und die Fehlermeldungen sind zusätzlich zur Animation sichtbar. "Show you screens". Authentizität.

***

## Synthese
Die meisten dieser Synthese-Verfahren gelten für Sound und Visuals in ähnlicher Weise. Diese Synthesetechniken sind für analoge und digitale Systeme geeignet.

### Additiv Synthese
Für Audio und Visuals.

Überlagerung von Schwingung unterschiedlicher Frequenz und Phase.

### Subtraktiv Synthese
#### Für Audio geeignet
Aus dem Rauschen durch Filter bestimmte Frequenzbänder auswählen.

#### Bei Visuals
Rauschfunktionen werden verwendet die keine hohen 'räumlichen' Frequenzen erzeugen.

* Beispiel: Perlin Noise

Oder Rauschfunktionen bei denen beeinflusst werden kann in wie weit hohe Frequenzen gewollt sind.

* Beispiel: Turbulenz auf Basis von Perlin Noise

### Granular Synthese
* "Sound Grains". für Audio.
* Stroboskop Effekte für Visuals.


## Quellen für Grafikdaten für Hydra
* generative "Oszillatoren" `osc()`
* Puffer innerhalb Hydras `src(o0)`
* Kameras `s0.initCam()`
 * emulierte Kameras "v4l2loopback" loop back devices `s0.initCam()`
* die Fenster anderer Applikationen `s0.initScreen()`
* Processing p5js `s0.init({ src: myp5.canvas })`
* und etliche andere Javascript Bibliotheken

### Rückkopplungsschleifen
Die Komplexität entsteht trotz einfacher Formeln durch die Rückkopplungsschleife.

Dies gilt ganz allgemein für Audio und Visuals.

### Transformation und Verzerrung der Koordinaten
Abbildung `R2 -> R2`

* skalieren, rotieren, ....

* In Kombination mit einer Rückkopplung entsteht eine „Mehrfachverkleinerungsmaschine“. Dieser Ansatz geht in Richtung Fraktale.

### Konstante Parameter / Lambda Funktionen als Parameter
Parameter mit einer Formel berechnen.

* Dann das Ergebnis übergeben.
  * Das Ergebnis wird als Konstante verwendet.

* Oder eine Funktion `() => ...` an eine Variable binden.
  * Hier ist das Ergebnis eine Funktion. Diese Funktion wird pro Frame aufgerufen.

Funktionale Programmiersprachen: Variable können Konstante oder (lambda) Funktionen enthalten.

```javascript
// 3+1 wird berechnet wenn diese Zeile im live coding
// an den Code Generator gesendet wird.
voronoi(5, 0, 3+1).out(o0)
```

```javascript
// 3+Math.sin(time) wird berechnet wenn diese Zeile im live coding
// an den Code Generator gesendet wird.
// Bei der Berechnung wird die globale Variable time berücksichtigt.
voronoi(5, 0, 3+Math.sin(time)).out(o0)
```

```javascript
// () => 3+Math.sin(time) wird als Funktion gebunden
// wenn diese Zeile im live coding an den Code Generator gesendet wird.
// Vor jedem Frame wird die Funktion neu berechnet.
voronoi(5, 0, () => 3+Math.sin(time)).out(o0)
```

***

## Andere (ähnliche) Systeme

### Paloma Kop
Analoge Video Sythesizer.

https://palomakop.tv

"From Material Space(Time) into Electronic Spaces(Time)" Diese Master Arbeit stammt aus dem analogen Signalverarbeitung. Aber es ist auf digitale (rückgekoppelte) Systeme übertragbar. Sehr empfehlenswert.
https://palomakop.s3.amazonaws.com/palomakop_thesis_book_digital.pdf

### scanlines
"Discussion group for diy video and audio projects." Digital und Analog.

https://scanlines.xyz

### analog Not analog
Live coding visuals. Basiert auf openFrameworks und Clojure.

https://metagrowing.org/

The 'analog Not analog' cookbook.

http://metagrowing.org/cookbook/

### SuperCollider
Für Sound Synthese und Analyse. Für Color to Sound Experimente. Oder als low frequency oscillator um grafische Animation zu steuern. SuperCollider kann auch als Live coding Tool verwendet werden.

https://supercollider.github.io/

### Tidal Cycles
Live coding sound. Basiert auf Haskel und SuperCollider.

https://tidalcycles.org/

### No Input Mixer Tutorial by Sarah Belle Reid
Ein analoger Audio Mixer wird als Sound Generator verwenet. Die Ausgänge des Mixers gehen als Rückkopplung in den Mixer zurück. Die analoge Elektronik erzeugt termisches Rauschen. Durch Rückkopplung und Filter entsteht Sound.

Diese Denkweise ist sehr gut auf visuelle Rückkopplungen übertragbar.

https://www.youtube.com/watch?v=oUhfkaVUPY8

### Sonic Pi
Live coding sound.
https://sonic-pi.net/

***

## Systemanforderungen für Hydra
* Ein Browser der WebGL unterstützt.
* Zugang zum Internet um Hydra zu starten.
* Alternativ: Hydra im Atom Editor.

***

## Links

Hydra ist hier zu finden:
https://hydra.ojack.xyz/

#### Olivia Jack
Olivia Jack hat Hydra entwickelt.
https://ojack.xyz/

#### Einführung in Hydra

https://hydra.ojack.xyz/docs/#/getting_started

https://github.com/hydra-synth/hydra

Beschreibung und Beispiele der Funktionen:
https://hydra.ojack.xyz/api/

Beschreibung der Funktionen:
https://github.com/ojack/hydra/blob/main/docs/funcs.md

#### hydra internet garden
https://hydra.ojack.xyz/garden/

#### Extensions
Sammlung zusätzliche Funktionen: https://gitlab.com/metagrowing/extra-shaders-for-hydra

hyper-hydra extensions: https://github.com/geikha/hyper-hydra/tree/main

#### Hydra für 3 Personen
"This is a collaborative console for hydra synth" https://pixeljam.glitch.me

#### Hydra Discord server
https://discord.gg/ZQjfHkNHXC

***
## Tastenkürzel
* `CTRL-Enter`: eine Zeile ausführen
* `CTRL-Shift-Enter`: den gesamten Code ausführen
* `CTRL-Shift-H`: Quelltext verbergen
* `CTRL-Shift-S`: Screenshot

***
## Beispiele
### Minimal Beispiel
Dies ist das minimale Beispiel. Ein Sinus-Oszillatror schreibt in Puffer `o0`.

![](md-images/hydra-2023-10-12-21.5.47.png)

```javascript
osc().out()
```
Der Sinus-Oszillator kann parametrisiert werden.

![](md-images/hydra-2023-10-12-21.29.37.png)

`osc( frequency, sync, offset )`

* `frequency` :: float (default `60.0`)
* `sync` :: float (default `0.1`)
* `offset` :: float (default `0.0`)

Dokumentation der Parameter: https://github.com/ojack/hydra/blob/main/docs/funcs.md

```javascript
osc(15, 0.1, 0.5).out()
```

### Geometrie
Das Koordinatensystem wird transformiert.

![](md-images/hydra-2023-10-12-21.40.48.jpg)

```javascript
render()
voronoi().out(o0)
voronoi().scale(0.5).kaleid().out(o1)
voronoi().repeat().out(o2)
voronoi().pixelate().out(o3)
```

### Farbe
Die Farbe wird manipuliert.

![](md-images/hydra-2023-10-26-19.48.24.jpg)

```javascript
render()
voronoi().out(o0)
voronoi().colorama(0.55).out(o1)
voronoi().color(1,0,3).out(o2)
voronoi().thresh().out(o3)
```

### Achtung Reihenfolge
0. Koordinatensystem
1. Transformation der Koordinaten
2. Koordinaten in Graustufen (oder Farbe) abbilden.
3. Farbe manipulieren

Es gibt ein Koordinatensystem. Dieses wird transformiert. Dann werden die Farben oder Graustufen aus den transformierten Koordinaten berechnet. Im letzten Schritt kann die Farbe verändert werden.

Achtung: Die Schreibweise suggeriert nicht unbedingt diese Reihenfolge.

```javascript
osc()              // 2. Koordinaten in Graustufen
	.rotate(0.7)      // 1. Transformation der Koordinaten
	.colorama(0.55)  // 3. Farbe manipulieren
	.out(o0)
```

![](md-images/hydra-2023-10-26-20.8.40.png)

### Kamera
Das Bild der Kamera einbinden.

![](md-images/camera-4.jpg)

```javascript
s0.initCam()
render()
src(s0).hue().out(o0)
src(s0).thresh(0.5).out(o1)
src(s0).shift().out(o2)
src(s0).color(1.7, 0.5, 0, 1).out(o3)
```
Falls es mehrere Kameras gibt, wird die gewünschte Kamera per Parameter ausgewählt: `0`, `1`, ...
* `s0.initCam(0)`
* `s0.initCam(1)`

```javascript
s0.initCam(0)
s1.initCam(1)
src(s0).blend(s1).out(o2)
render(o2)
```

### Forward Pipeline
Die Grafikdaten fließen ohne Rückkoplungsschleifen durch die Grafikpuffer.

A --> B "Gerichteter azyklischer Graph"

#### Upstream überlagert
Diese Funktionen ähneln den 'Blending Modes' in Grafikprogrammen um 'Layer' zu verknüpfen.

![](md-images/hydra-2023-10-12-21.43.13.jpg)

```javascript
render()
osc(4, 0.5, 5).rotate(() => time).out(o0)
osc(4, () => 0.5+0.1*Math.sin(0.01*time), 2).rotate(0.5).out(o1)
src(o0).diff(src(o1).repeat(10,1)).out(o2)
src(o0).blend(noise(3)).out(o3)
```

Datenfluss vereinfacht, die geometrischen Operation sind hier weggelassen.

![](flow/upstream_ueberlagert_small.png)

#### Der Upstream moduliert
Die Denkweise der Modulatoren stammt aus der Welt der analogen modularen Synthesizer. Beispiel: Ein sehr niederfrequenter Oszillator (LFO) moduliert die Frequenz eines Sinusgenerators der im hörbaren Bereich schwingt.

![](md-images/hydra-2023-10-13-6.40.8.jpg)

```javascript
render()
osc(5, 0.5, 5).rotate(() => time).out(o0)
osc(4, () => 0.5+0.1*Math.sin(0.01*time), 2).modulateKaleid(voronoi()).out(o1)
src(o0).pixelate().modulatePixelate(src(o1)).out(o2)
src(o0).modulateRotate(noise()).out(o3)
```
![](flow/upstream_moduliert_small.png)

### Feedback
Es gibt Rückkopplungsschleifen zwischen den Puffern.

A <--> B "Gerichteter Graph, Zyklen sind erlaubt"

Mit Rückkopplungsschleifen sind die Effekte nicht leicht kontrollierbar, aber sehr interessant.

#### Feedback mit externer Kamera
Was geschieht wenn man die Kamera auf den Monitor richtet?

![](md-images/hydra-2023-10-14-12.51.55.jpg)

```javascript
s0.initCam()
//src(s0).out(o0)
src(s0).colorama(0.3).blend(o0, 0.95).out(o0)
```
![](flow/feedback_extern_1_small.png)

#### Internes Feedback
Kurze Rückkopplungsschleife mit dem eigenen Vorzustand. Die Puffer werden mit den "invertierten" Vorzustand oder mit der "Differnz" des Vorzustandes gekoppelt.

![](md-images/Screenshot_20231118_202057.jpg)

```javascript
s1.initCam()

render()

render(o3)

src(s1).out(o1)
src(o1).scale(3).diff(src(o2).scale(0.97).rotate(1.5)).out(o2)
src(o2).blend(src(o3), 0.95).out(o3)
```
![](flow/feedback_intern_1_small.png)

#### Feedback: Rauschen unterdrücken
Die Kurze Rückkopplungsschleife am Ende der Pipeline unterdrückt das Rauschen.

`src(o2).blend(src(o3), 0.95).out(o3)` ähnelt einem Tiefpass. Nur 5% des aktuellen "upstream" Bildes `o2` werden mit 95% des alten Zustandes aus `o3` kombiniert und in den Akku `o3` geschrieben.

![](md-images/hydra-2023-10-14-13.1.48.jpg)

```javascript
render(o3)
shape(()=>3+2.5*Math.sin(0.31*time)).colorama(0.2).saturate(0.7).hue(()=>Math.sin(time)).out(o0)
src(o0).scale(3).diff(src(o2).scale(0.97).rotate(1.5)).out(o2)
src(o2).blend(src(o3), 0.95).out(o3)
```
![](flow/feedback_2_sources_small.png)

### Feedback mit Modulator

![](md-images/hydra-2023-10-14-16.15.10.jpg)

Auch der Ansatz in der Rückkopplung die graphischen Daten mit Modulatoren zu manipulieren funktioniert.

Dieses Beispiel erzeugt Rauschen. Es enthält keine Dämpfung.

```javascript
render()
osc(7, 0.4, 2).diff(src(o1)).saturate(0.3).hue(0.5).out(o0)
osc(7, 0.4, 2).modulateRepeat(src(o2).rotate(0.3)).saturate(0.3).hue(0.05).out(o1)
osc(7, 0.4, 2).modulateRepeat(src(o3).rotate(0.3).color(1.5)).saturate(0.3).hue(0.05).out(o2)
src(o2).modulateRotate(src(o0).scale(0.25).repeat(2)).colorama(0.5).out(o3)
```
![](flow/feedback_intern_modulator_small.png)

### Fraktale per Feedback erzeugen
30 Jahre zurück: "Mehrfach-Verkleinerungs-Kopier-Maschine"

![](md-images/Screenshot_20231022_201859.png)

```javascript
//s0.initCam()
//src(s0).out(o0)

osc()
	.thresh(0.1)
	.out(o0)

src(o0)
	.mask(shape(5, 0.95))
	.invert()
	.rotate()
	.scale(0.99)
	.out(o0)

src(o0)
	.mask(shape(3, 1.1))
	.colorama(0.01)
	.rotate(() => 0.05 * time)
	.scale(() => 0.95 + 0.04 * Math.sin(0.09 * time))
	.scrollY(0.05)
	.out(o0)
  ```

### Zusätzliche Fragment-Shader definieren
#### Motivation
* zusätzliche Muster
* zusätzliche Farb-Mixer
* einen eigenen unverwechselbaren Stil zeigen

#### Randbedingungen
* setFunction definiert WebGL 1.0 Fragment Shader
* Der zusätzliche Code muss genau in das Schema des Hydra Codegenerators passen.

#### GLSL Fragment Shader Code
Wenn man mit `setFunction()` eigenen FragmentShader Code entwickelt, so muss dieser syntaktisch genau zu dem von `hydra-synth` generierten Fragment Shader code passen.

Eine benutzerdefiniert Funktion zu erstellen bedeutet nicht, dass man eine beliebige GLSL-Funktion erstellen kann.

Funktionen werden für den Hydra Code Generator in 5 unterschiedliche Typen eingeteilt.
* src
* coord
* color
* combine
* combineCoords

Hier sind vordefinierten Funktion zu finden. Für den Einstieg diese als Vorlage verwenden. https://github.com/hydra-synth/hydra-synth/blob/main/src/glsl/glsl-functions.js

#### Beispiel: konzentrische Ringe

![](md-images/hydra-2023-10-14-17.48.12.jpg)

```javascript
setFunction({
  name: 'concentric',
  type: 'src',
  inputs: [
    {name: 'base',     type: 'float', default: 5.0},
    {name: 'octaves',  type: 'float', default: 2.0},
    {name: 'ampscale', type: 'float', default: 0.5},
    {name: 'speed',    type: 'float', default: 1.0},
  ],
  glsl: `
  float r = base*length(_st - vec2(0.5));
  float d = 0.5*sin(r+speed*time) + 0.5;
  d += ampscale*(0.5*sin(r*octaves+speed*time) + 0.5);
  octaves *= octaves;
  ampscale *= ampscale;
  d += ampscale*(0.5*sin(r*octaves) + 0.5);
  octaves *= octaves;
  ampscale *= ampscale;
  d += ampscale*(0.5*sin(r*octaves+speed*time) + 0.5);
  return vec4(d, d, d, 1);
`})

render(o0)
concentric(30).out(o0)

render(o1)
concentric(5,2,0.5).modulate(concentric(20,2,0.5)).out(o1)

render(o2)
concentric(5,4,0.25).color(0.7,0,0).add(concentric(3,2,0.5).scrollY(0.1).color(0.0,0.6,0)).out(o2)

render(o3)
concentric(30,2,0.5,1.31).colorama(0.5).modulateRepeat(concentric(30,2,0.5,0.51),0.1,0.1,0,0.5).out(o3)

render()
```

#### 2 Wege um GLSL Code auszugeben

![](md-images/dump-shader-source.png)

Den generierten Quelltext in die WEB-Console schreiben.
```javascript
shader = osc(30, 0.1, 0.2).colorama(0.7).glsl(o0)
console.log(shader[0].frag)
```

Den generierten Quelltext im Browser anzeigen.
```javascript
await loadScript("https://hyper-hydra.glitch.me/hydra-debug.js")
osc().colorama().debug(o0)
```


### Bibliotheken mit zusätzlichen Fragment-Shadern

![](md-images/Screenshot_20231014_175608.jpg)

```javascript
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")



blobs(0.13, 0.2, 0.2)
	.modulate(blobs(0.21, 0.5, 0.2)
		.modulate(blobs(0.23, 0.9, 0.2), 1), 1)
	.shift()
	.out(o0)

render(o0)
```
https://hydra.ojack.xyz/dev/

https://gitlab.com/metagrowing/extra-shaders-for-hydra

![](md-images/hydra_library.png)


### Processing / Integration p5js in Hydra

![](md-images/hydra-2023-10-16-12.42.38.jpg)

```javascript
// see: https://p5js.org/examples/math-parametric-equations.html
setResolution(640, 640)
if(typeof myp5 === 'undefined') {
  // myp5 is globally visible
  // only one constructor call in case of rerunning this sketch
  myp5 = new P5({mode: 'WEBGL'})
}
let t = 0
let sl = 250
myp5.hide() //hide p5js canvas.

// the parameter at which x and y depends is usually taken as either t or symbol of theta
myp5.draw = () => {
  myp5.background('#e0e0e0');
  myp5.strokeWeight(3);
  myp5.stroke('#00ffff');
  myp5.line(x1(t), y1(t), x2(t) + 20, y2(t) + 20);
  myp5.stroke('#0f0f0f');
  for(let i = 1;i<100;i++){
    myp5.line(x1(t+i), y1(t+i), x2(t+i) + 20, y2(t+i) + 20);
  }
  t += 0.15;
}
// function to change initial x co-ordinate of the line
function x1(t){
  return Math.sin(t / 10) * sl + Math.sin(t / 20) * sl + Math.sin(t / 30) * sl;
}

// function to change initial y co-ordinate of the line
function y1(t){
  return Math.cos(t / 10) * sl + Math.cos(t / 20) * sl + Math.cos(t / 30) * sl;
}

// function to change final x co-ordinate of the line
function x2(t){
  return Math.sin(t / 15) * sl + Math.sin(t / 25) * sl + Math.sin(t / 35) * sl;
}

// function to change final y co-ordinate of the line
function y2(t){
  return Math.cos(t / 15) * sl + Math.cos(t / 25) * sl + Math.cos(t / 35) * sl;
}

s0.init({ src: myp5.canvas })
render(o0)
src(s0).out(o0)

render(o1)
src(s0).scale(0.5).kaleid(5).out(o1)

render(o2)
src(s0).diff(o1).out(o2)

render(o3)
src(o2).blend(o3, 0.9).out(o3)
```

### Animation reagiert auf Sound
Als Quelle dient das Mikrophon des Laptops.

FFT "Fast Fourier Transformation": Das Audiosignal wird in seine Frequenzbestanteile zerlegt. Spektralkomponenten.

Hydra verwendet in der Grundeinstellung 4 Frequenzbänder. `a.fft[0]` ist das tiefste Frequenzband. Details sind hier zu finden: https://github.com/hydra-synth/hydra#audio-responsiveness

![](md-images/Screenshot_20231028_204430.png)

```javascript
a.show()
shape(3)
	.modulateRotate(noise(), () => 5 * a.fft[0])
	.out(o0)
```

```javascript
voronoi(6, 3)
	.modulateKaleid(noise(1,() => 0.0005 * a.fft[0]), 13)
	.colorama(() => a.fft[3])
	.invert()
	.out(o0)
```

```javascript
let gain = 3
voronoi(5, () => 0.005 * a.fft[0], 7)
	.modulateScale(noise(), () => a.fft[0] * a.fft[1])
	.color(() => gain * 7 * a.fft[1],
           () => gain * 5 * a.fft[2],
           () => gain * 2 * a.fft[3])
	.out(o0)
```

***

Author Thomas Jourdan: https://chaos.social/@kandid

Lizenz: CC BY-NC-SA 4.0
